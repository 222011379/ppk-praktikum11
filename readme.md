Nama : Maulana Pandudinata
NIM : 222011379
Kelas : 3SI3

Praktikum Pemrograman Platform Khusus Pertemuan 11

- Dokumentasi Praktikum
![btn1](dokumentasi/btn1.jpg)
![btn2](dokumentasi/btn2.jpg)

1. Penugasan 1
    - Tampilan setelah di klik Button 3
    ![no1](dokumentasi/no1.jpg)

2. Penugasan 2
    - Tampilan setelah di klik Button 3
    ![no2](dokumentasi/no2.jpg)

3. Penugasan 3
    - Bagian Awal Form
    ![input](dokumentasi/input.jpg)
    - Input form
    ![input2](dokumentasi/input2.jpg)
    - Tampilan Output
    ![output](dokumentasi/output.jpg)

